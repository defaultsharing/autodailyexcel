# coding=utf-8

from datetime import date, timedelta
import pandas as pd
import numpy as np
import json
import urllib.request
import urllib.parse

MAHOZ_NAME = 'מחוז ירושלים והמרכז'
SETTLEMENTS = ['אבו גוש','בית שמש','הר אדר','מטה יהודה','מבשרת ציון','קרית יערים','אלעד','בית דגן','לוד','חבל מודיעין','שדות דן','מודיעין - מכבים - רעות','פתח תקווה','ראש העין','שהם','גבעת זאב','ירושלים','מעלה אדומים','באר יעקב','גדרה','גן רוה','יבנה','ברנר','גדרות','גזר','חבל יבנה','נחל שורק','מזכרת בתיה','נס ציונה','קרית עקרון','ראשון לציון','רחובות','רמלה']
#SETTLEMENTS = ['ברנר', 'רחובות']

SICK_URL = "https://utility.arcgis.com/usrsvcs/servers/85c0c44ac4fa4db9aaa932ab7b43ca04/rest/services/COR/COR_Setl_Health_Rate_All_Dates/MapServer/0/query?f=json&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&outSR=102100"
HEALTH_URL = "https://utility.arcgis.com/usrsvcs/servers/bb6b693c28454b68a723691be98323b5/rest/services/COR/COR_Setl_Health_Rate_All_Dates/MapServer/1/query?f=json&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&outSR=102100"
RAMZOR_URL = "https://utility.arcgis.com/usrsvcs/servers/ae298cecc6b54405a9fd27629cb0d520/rest/services/COR/COR_RAMZOR/MapServer/3/query?f=json&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&outSR=102100"
NEWSICK_URL = "https://utility.arcgis.com/usrsvcs/servers/af02f9bb8c4745f29f551420e6cc49a8/rest/services/COR/COR_Setl_Health_Rate_All_Dates/MapServer/5/query?f=json&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&outSR=102100"
VACC_URL = "https://utility.arcgis.com/usrsvcs/servers/88593da2d2514dd986fb05f19d75ca66/rest/services/COR/COR_VAC_INFO_1011/MapServer/0/query?f=json&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*"

SICK_FIELD_OUTSTATS = '[{"statisticType":"sum","onStatisticField":"active_sick","outStatisticFieldName":"value"}]'
MR7_FIELD_OUTSTATS = '[{"statisticType":"avg","onStatisticField":"multiply_rate_7","outStatisticFieldName":"value"}]'
SICK10K_FIELD_OUTSTATS = '[{"statisticType":"avg","onStatisticField":"sick_for_10000","outStatisticFieldName":"value"}]'
ISOLATED_FIELD_OUTSTATS = '[{"statisticType":"sum","onStatisticField":"active_isolations","outStatisticFieldName":"value"}]'
TESTS_FIELD_OUTSTATS = '[{"statisticType":"sum","onStatisticField":"add_test_for_positive_yesterday","outStatisticFieldName":"value"}]'
NEWSICK_FIELD_OUTSTATS = '[{"statisticType":"sum","onStatisticField":"add_sick","outStatisticFieldName":"value"}]'
RAMZOR_FIELD_OUTSTATS = '[{"statisticType":"avg","onStatisticField":"final_score","outStatisticFieldName":"value"}]'
TOTRECOV_FIELD_OUTSTATS = '[{"statisticType":"sum","onStatisticField":"total_recovery","outStatisticFieldName":"value"}]'
VACC_FIELD_OUTSTATS = '[{"statisticType":"sum","onStatisticField":"vaccina_today","outStatisticFieldName":"value"}]'
VACCALL_FIELD_OUTSTATS = '[{"statisticType":"sum","onStatisticField":"vaccina_all","outStatisticFieldName":"value"}]'

DATE_GROUP_FIELD = 'update_date_text'
DATE_ORDER = 'update_date_text asc'

ADDDEAD_FIELD_OUTSTATS = '[{"statisticType":"sum","onStatisticField":"add_dead","outStatisticFieldName":"value"}]'
ADDRECOVERY_FIELD_OUTSTATS = '[{"statisticType":"sum","onStatisticField":"add_recovery","outStatisticFieldName":"value"}]'
ADDSICK_FIELD_OUTSTATS = '[{"statisticType":"sum","onStatisticField":"add_sick","outStatisticFieldName":"value"}]'

token = "emnPWVzCdabtnykJbCDARXEvRRQENTKpgwBbTVW6kashgQKVlx4H2UQ2ozhyBEsBFvjrrTwZPscJFdh4SfTPs1hu5EAYzei_FA9h_l83KNK8d592Qyaql_iv6Fvf7UDMVPiE3tLN2I8NDdh5GJIk18gIZg7JfiIGjqJVh-PBy9b92kCYq6KvzpGjHB6saVWYTlb6E5nFUidYXjcqEfs3B8vOzxsSJsPAQFZYm6vZBbNPJs1yBPmLCCeBc8YSkOHNbGdTisgGNaQ2MxEI36mTwQ.."



def getWhere(mahozName, settleName):
    result = ""
    if (mahozName != None and mahozName != ""):
        result = "(MahoznamePQR='" + mahozName + "')"

    if (settleName != None and settleName != ""):
        if (result != ""):
            result = result + " AND "

        result = result + "(Municipal_authoritie='" + settleName + "')"

    return result


def getArray(templateUrl, whereExpr, token, outStatistics, group, order, default):
    try:
        jsonUrl = templateUrl + "&outStatistics=" + urllib.parse.quote(outStatistics)
        jsonUrl = jsonUrl + "&groupByFieldsForStatistics=" + urllib.parse.quote(group)
        jsonUrl = jsonUrl + "&orderByFields=" + urllib.parse.quote(order)
        jsonUrl = jsonUrl + "&where=" + urllib.parse.quote(whereExpr) + "&token=" + token
        with urllib.request.urlopen(jsonUrl) as url:
            data = json.loads(url.read().decode())
            if len(data["features"]) <= 0:
                return default
                
            return data["features"]
    except:
        return default


def getValue(templateUrl, whereExpr, token, outStatistics, default):
    try:
        jsonUrl = templateUrl + "&outStatistics=" + urllib.parse.quote(outStatistics) + "&where=" + urllib.parse.quote(whereExpr) + "&token=" + token
        with urllib.request.urlopen(jsonUrl) as url:
            data = json.loads(url.read().decode())
            if len(data["features"]) <= 0:
                return default

            return data["features"][0]["attributes"]["value"]
    except:
        return default


def getFeatures(templateUrl, whereExpr, token, default):
    try:
        jsonUrl = templateUrl + "&where=" + urllib.parse.quote(whereExpr) + "&token=" + token
        with urllib.request.urlopen(jsonUrl) as url:
            data = json.loads(url.read().decode())
            return data["features"]
    except:
        return default


def getRow(token, mahozName, settleName):
    whereExpr = getWhere(mahozName, settleName)

    settleFeatures = getFeatures(HEALTH_URL, whereExpr, token, {})
    if (settleFeatures.__len__() == 0):
        population = None
    else:
        population = 0
        for f in settleFeatures:
            population += f["attributes"]["civil"]
            

    recArr = getArray(SICK_URL, whereExpr, token, ADDRECOVERY_FIELD_OUTSTATS, DATE_GROUP_FIELD, DATE_ORDER, None)
    if (recArr == None):
        recov = None
    else:
        recov = recArr[recArr.__len__()-2]["attributes"]["value"]

    deaArr = getArray(SICK_URL, whereExpr, token, ADDDEAD_FIELD_OUTSTATS, DATE_GROUP_FIELD, DATE_ORDER, None)
    if (deaArr == None):
        dead = None
    else:
        dead = deaArr[deaArr.__len__()-2]["attributes"]["value"]

    today = date.today()

    rExpr = whereExpr 
    if (rExpr != ""):
        rExpr = rExpr + " AND "

    rExpr = rExpr + "(city_code<>926 AND day_date BETWEEN timestamp '" + today.strftime("%Y-%m-%d") + " 00:00:00' AND CURRENT_TIMESTAMP)"

    ramzorArr = getArray(RAMZOR_URL, rExpr, token, RAMZOR_FIELD_OUTSTATS, DATE_GROUP_FIELD, DATE_ORDER, None)
    if (ramzorArr == None):
        ramzor = None
    else:
        ramzor = ramzorArr[ramzorArr.__len__()-1]["attributes"]["value"]


    rExpr = whereExpr
    if (rExpr != ""):
        rExpr = rExpr + " AND "

    rExpr = rExpr + "(multiply_rate_7<>0)"

    multiply_rate_7 = getValue(HEALTH_URL, rExpr, token, MR7_FIELD_OUTSTATS, None)


    today_minus_2 = today + timedelta(days=-2)
    today_minus_1 = today + timedelta(days=-1)

    rExpr = getWhere(mahozName, settleName)
    if (rExpr != ""):
        rExpr = rExpr + " AND "

    rExpr = rExpr + "(update_date BETWEEN timestamp '" + today_minus_2.strftime("%Y-%m-%d") + " 22:00:00' AND '" + today_minus_1.strftime("%Y-%m-%d") + " 21:59:59')"
    vacc_last = getValue(VACC_URL, rExpr, token, VACC_FIELD_OUTSTATS, None)
    vacc_all = getValue(VACC_URL, rExpr, token, VACCALL_FIELD_OUTSTATS, None)

    alias = settleName
    if (alias == None):
        if (mahozName != None):
            alias = mahozName
        else:
            alias = "כלל המדינה"

    row = {
        'settlement': alias,
        'ramzor': ramzor,
        'sick': getValue(HEALTH_URL, whereExpr, token, SICK_FIELD_OUTSTATS, None),
        'isolated': getValue(HEALTH_URL, whereExpr, token, ISOLATED_FIELD_OUTSTATS, None),
        'multiply_rate_7': multiply_rate_7,
        'sick_10k': getValue(HEALTH_URL, whereExpr, token, SICK10K_FIELD_OUTSTATS, None),
        'tests': getValue(HEALTH_URL, whereExpr, token, TESTS_FIELD_OUTSTATS, None),
        'new_sick': getValue(NEWSICK_URL, whereExpr, token, NEWSICK_FIELD_OUTSTATS, None),
        'recov_yesterday': recov,
        'dead_yesterday': dead,
        'recov': getValue(HEALTH_URL, whereExpr, token, TOTRECOV_FIELD_OUTSTATS, None),
        'population': population,
        'vacc_last': vacc_last,
        'vacc_all': vacc_all
    }

    return row

today = date.today()
fileName = today.strftime("%Y%m%d")

rows_list = []

#print (getRow(token, MAHOZ_NAME, "ירושלים"))
#print (getRow(token, MAHOZ_NAME, None))
#print (getRow(token, None, None))

print('started')
for settleName in SETTLEMENTS:
    print('querying ' + settleName)
    rows_list.append(getRow(token, MAHOZ_NAME, settleName))
    print(settleName + ' OK')

print('querying ' + MAHOZ_NAME)
rows_list.append(getRow(token, MAHOZ_NAME, None))
print(settleName + ' OK')

print('querying entire country')
rows_list.append(getRow(token, None, None))
print(settleName + ' OK')

mahoz = pd.DataFrame(rows_list)
print(mahoz)

mahoz.to_excel("./Reports/Mahoz_" + fileName + ".xlsx", sheet_name="מחוז ירושלים והמרכז", index=False)