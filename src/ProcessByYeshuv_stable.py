# coding=utf-8

from datetime import date
import pandas as pd
import numpy as np
import json
import urllib.request
import urllib.parse

MAHOZ_NAME = 'מחוז ירושלים והמרכז'
SETTLEMENTS = ['אביעזר','אבן ספיר','אדרת','אורה','איתנים','אשתאול','בית זית','בית מאיר','בית נקופה','בקוע','בר גיורא','גבעת יערים','גבעת ישעיהו','גבעת שמש','גיזו','גפן','דייר ראפאת','הראל','זכריה','זנוח','טל שחר','יד השמונה','ידידה','ישעי','כסלון','כפר אוריה','כפר זוהרים','לוזית','מבוא ביתר','מוצא עילית','מחסיה','מטע','מסילת ציון','מעלה החמישה','נווה אילן','נווה מיכאל','נווה שלום','נחושה','נחם','נחשון','נטף','נס הרים','נתיב הל"ה','עגור','עין כרם-בי"ס חקלאי','עין נקובא','עין ראפה','עמינדב','צובה','צור הדסה','צלפון','צפרירים','צרעה','קרית יערים(מוסד)','קרית ענבים','רמת רזיאל','רמת רחל','שדות מיכה','שואבה','שורש','שריגים (לי-און)','תירוש','תעוז','תרום','אחיסמך','בארות יצחק','בית נחמיה','בן שמן (כפר נוער)','בן שמן (מושב)','בני עטרות','ברקת','גבעת כ"ח','גמזו','גנתון','חדיד','טירת יהודה','כפר דניאל','בית עריף','כפר טרומן','כפר רות','כרם בן שמן','לפיד','מבוא מודיעים','מזור','נופך','נחלים','רינתיה','שילת','אחיעזר','זיתן','ניר צבי','צפריה','חמד','גנות','משמר השבעה','יגל','כפר חב"ד','בית חשמונאי','בית עוזיאל','גזר','גני הדר','גני יוחנן','חולדה','יד רמב"ם','יציץ','ישרש','כפר ביל"ו','כפר בן נון','כפר שמואל','כרמי יוסף','מצליח','משמר איילון','משמר דוד','נוף איילון','נען','נצר סרני','סתריה','עזריה','פדיה','פתחיה','רמות מאיר','שעלבים','גבעת ברנר','גן שלמה','בית אלעזרי','בניה','גיבתון','קדרון','מחנה תל נוף','בית חלקיה','בני ראם','גני טל','חפץ חיים','יד בנימין','יסודות','נצר חזני','בני דרום','גבעת וושינגטון','צופיה','בית גמליאל','ניר גלים','כרם ביבנה','קבוצת יבנה','בן זכאי','גן הדרום','כפר אביב','כפר מרדכי','מישר','משגב דב','שדמה','עשרת','פלמחים','בית חנן','גאליה','בית עובד','גן שורק','כפר הנגיד','נטעים','עיינות','אירוס']
#SETTLEMENTS = ['כפר חב"ד']

SICK_URL = "https://utility.arcgis.com/usrsvcs/servers/85c0c44ac4fa4db9aaa932ab7b43ca04/rest/services/COR/COR_Setl_Health_Rate_All_Dates/MapServer/0/query?f=json&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&outSR=102100"
HEALTH_URL = "https://utility.arcgis.com/usrsvcs/servers/bb6b693c28454b68a723691be98323b5/rest/services/COR/COR_Setl_Health_Rate_All_Dates/MapServer/1/query?f=json&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&outSR=102100"
RAMZOR_URL = "https://utility.arcgis.com/usrsvcs/servers/ae298cecc6b54405a9fd27629cb0d520/rest/services/COR/COR_RAMZOR/MapServer/3/query?f=json&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&outSR=102100"
NEWSICK_URL = "https://utility.arcgis.com/usrsvcs/servers/af02f9bb8c4745f29f551420e6cc49a8/rest/services/COR/COR_Setl_Health_Rate_All_Dates/MapServer/5/query?f=json&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&outSR=102100"

SICK_FIELD_OUTSTATS = '[{"statisticType":"sum","onStatisticField":"active_sick","outStatisticFieldName":"value"}]'
MR7_FIELD_OUTSTATS = '[{"statisticType":"avg","onStatisticField":"multiply_rate_7","outStatisticFieldName":"value"}]'
SICK10K_FIELD_OUTSTATS = '[{"statisticType":"avg","onStatisticField":"sick_for_10000","outStatisticFieldName":"value"}]'
ISOLATED_FIELD_OUTSTATS = '[{"statisticType":"sum","onStatisticField":"active_isolations","outStatisticFieldName":"value"}]'
TESTS_FIELD_OUTSTATS = '[{"statisticType":"sum","onStatisticField":"add_test_for_positive_yesterday","outStatisticFieldName":"value"}]'
NEWSICK_FIELD_OUTSTATS = '[{"statisticType":"sum","onStatisticField":"add_sick","outStatisticFieldName":"value"}]'
RAMZOR_FIELD_OUTSTATS = '[{"statisticType":"avg","onStatisticField":"final_score","outStatisticFieldName":"value"}]'
TOTRECOV_FIELD_OUTSTATS = '[{"statisticType":"sum","onStatisticField":"total_recovery","outStatisticFieldName":"value"}]'

DATE_GROUP_FIELD = 'update_date_text'
DATE_ORDER = 'update_date_text asc'

ADDDEAD_FIELD_OUTSTATS = '[{"statisticType":"sum","onStatisticField":"add_dead","outStatisticFieldName":"value"}]'
ADDRECOVERY_FIELD_OUTSTATS = '[{"statisticType":"sum","onStatisticField":"add_recovery","outStatisticFieldName":"value"}]'
ADDSICK_FIELD_OUTSTATS = '[{"statisticType":"sum","onStatisticField":"add_sick","outStatisticFieldName":"value"}]'

token = "RD7uCS_zvk8wlTaIraEQoHeYx70J-Mp8TE0oV8QFQqrRE0HTo7j_kv2ELHLRB3J__6FgMQhG5viiGMifQGvHTPtMPI8kZajIk9PFr8j3HCbangsG16MFPoC_m0i10D2IZ0eRUK8rnXHL_iVL27wQ-u2yIp9cjSA5FkfTFY2A0KquxMPQESpwFVAGEuBFbE4VDkfFTL79k64Bqi_YQ35rBeY4n_jSccQ_vsqt7k0HE9erLKLfyUSWM2UfY9jckKcVWhMfTvPULO7tSDri2m-XjQ.."

def getWhere(mahozName, settleName):
    result = ""
    if (mahozName != None and mahozName != ""):
        result = "(MahoznamePQR='" + mahozName + "')"

    if (settleName != None and settleName != ""):
        if (result != ""):
            result = result + " AND "

        result = result + "(SETL_NAME='" + settleName + "')"

    return result

def getWhereForCorRamzor(mahozName, settleName):
    result = ""
    if (mahozName != None and mahozName != ""):
        result = "(MahoznamePQR='" + mahozName + "')"

    if (settleName != None and settleName != ""):
        if (result != ""):
            result = result + " AND "

        result = result + "(city_desc='" + settleName + "')"

    return result

def getArray(templateUrl, whereExpr, token, outStatistics, group, order, default):
    try:
        jsonUrl = templateUrl + "&outStatistics=" + urllib.parse.quote(outStatistics)
        jsonUrl = jsonUrl + "&groupByFieldsForStatistics=" + urllib.parse.quote(group)
        jsonUrl = jsonUrl + "&orderByFields=" + urllib.parse.quote(order)
        jsonUrl = jsonUrl + "&where=" + urllib.parse.quote(whereExpr) + "&token=" + token
        with urllib.request.urlopen(jsonUrl) as url:
            data = json.loads(url.read().decode())
            if len(data["features"]) <= 0:
                return default

            return data["features"]
    except:
        return default


def getValue(templateUrl, whereExpr, token, outStatistics, default):
    try:
        jsonUrl = templateUrl + "&outStatistics=" + urllib.parse.quote(outStatistics) + "&where=" + urllib.parse.quote(whereExpr) + "&token=" + token
        with urllib.request.urlopen(jsonUrl) as url:
            data = json.loads(url.read().decode())
            return data["features"][0]["attributes"]["value"]
    except:
        return default


def getFeatures(templateUrl, whereExpr, token, default):
    try:
        jsonUrl = templateUrl + "&where=" + urllib.parse.quote(whereExpr) + "&token=" + token
        with urllib.request.urlopen(jsonUrl) as url:
            data = json.loads(url.read().decode())
            return data["features"]
    except:
        return default


def getRow(token, mahozName, settleName):
    whereExpr = getWhere(mahozName, settleName)

    settleFeatures = getFeatures(HEALTH_URL, whereExpr, token, {})
    if (settleFeatures.__len__() == 0):
        population = None
    else:
        population = 0
        for f in settleFeatures:
            population += f["attributes"]["civil"]
        

    recArr = getArray(SICK_URL, whereExpr, token, ADDRECOVERY_FIELD_OUTSTATS, DATE_GROUP_FIELD, DATE_ORDER, None)
    if (recArr == None):
        recov = None
    else:
        recov = recArr[recArr.__len__()-2]["attributes"]["value"]

    deaArr = getArray(SICK_URL, whereExpr, token, ADDDEAD_FIELD_OUTSTATS, DATE_GROUP_FIELD, DATE_ORDER, None)
    if (deaArr == None):
        dead = None
    else:
        dead = deaArr[deaArr.__len__()-2]["attributes"]["value"]

    today = date.today()
    rExpr = getWhereForCorRamzor(mahozName, settleName) 
    if (rExpr != ""):
        rExpr = rExpr + " AND "

    rExpr = rExpr + "(city_code<>926 AND day_date BETWEEN timestamp '" + today.strftime("%Y-%m-%d") + " 00:00:00' AND CURRENT_TIMESTAMP)"

    ramzorArr = getArray(RAMZOR_URL, rExpr, token, RAMZOR_FIELD_OUTSTATS, DATE_GROUP_FIELD, DATE_ORDER, None)
    if (ramzorArr == None):
        ramzor = None
    else:
        ramzor = ramzorArr[ramzorArr.__len__()-1]["attributes"]["value"]


    rExpr = whereExpr
    if (rExpr != ""):
        rExpr = rExpr + " AND "

    rExpr = rExpr + "(multiply_rate_7<>0)"

    multiply_rate_7 = getValue(HEALTH_URL, rExpr, token, MR7_FIELD_OUTSTATS, None)


    alias = settleName
    if (alias == None):
        if (mahozName != None):
            alias = mahozName
        else:
            alias = "כלל המדינה"

    row = {
        'settlement': alias,
        'ramzor': ramzor,
        'sick': getValue(HEALTH_URL, whereExpr, token, SICK_FIELD_OUTSTATS, None),
        'isolated': getValue(HEALTH_URL, whereExpr, token, ISOLATED_FIELD_OUTSTATS, None),
        'multiply_rate_7': multiply_rate_7,
        'sick_10k': getValue(HEALTH_URL, whereExpr, token, SICK10K_FIELD_OUTSTATS, None),
        'tests': getValue(HEALTH_URL, whereExpr, token, TESTS_FIELD_OUTSTATS, None),
        'new_sick': getValue(NEWSICK_URL, whereExpr, token, NEWSICK_FIELD_OUTSTATS, None),
        'recov_yesterday': recov,
        'dead_yesterday': dead,
        'recov': getValue(HEALTH_URL, whereExpr, token, TOTRECOV_FIELD_OUTSTATS, None),
        'population': population,
        'vaccined': -1
    }

    return row

today = date.today()
fileName = today.strftime("%Y%m%d")

rows_list = []

print('started')
for settleName in SETTLEMENTS:
    print('querying ' + settleName)
    rows_list.append(getRow(token, None, settleName))
    print(settleName + ' OK')

mahoz = pd.DataFrame(rows_list)
print(mahoz)

mahoz.to_excel("./Reports/Yeshuvim_" + fileName + ".xlsx", sheet_name="ישובים", index=False)